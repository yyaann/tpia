(function(){

    

    /* circle popup */
    const circles = getElements('.circle') // 抓到圈圈
    const popupBox = getElement('.popup_box')
    const popup = getElement('.popup') // 白圈
    const boxes = getElements('.box') // 對應的彈窗內容

    popup.addEventListener('click',(event)=> {
        event.stopPropagation();
    })

    circles.forEach((circle,index) => {
        circle.addEventListener('click', ()=> {            
            popupBox.classList.add('active')
            popup.classList.add('active')
            boxes[index].classList.add('active')
        })
    })

    popupBox.addEventListener('click', ()=> {
        popupBox.classList.remove('active')
        popup.classList.remove('active')
        boxes.forEach(i => {
            i.classList.remove('active')
        })    
    })

    /* video player popup */
    /*
    const playBtn = getElement('.play_box_triangle')
    const videoPlayBox = getElement('.video_play_box')

    playBtn.addEventListener('click', function(){
        const closeBtn = getElement('.close')
        videoPlayBox.classList.toggle('active')

        closeBtn.addEventListener('click', function() {
            const video = getElement('video')
            video.pause() // 每次關閉都會將影片暫停並歸零進度
            video.currentTime = 0
            videoPlayBox.classList.remove('active')
        })
    })
    */

    /* iframe player popup */
    const playBtn = getElement('.play_box_triangle')
    const videoPlayBox = getElement('.video_play_box')

    playBtn.addEventListener('click', function(){
        const closeBtn = getElement('.close')

        videoPlayBox.classList.toggle('active')

        closeBtn.addEventListener('click', function() {
            const iframe = getElement('iframe')
            const iframeSrc = iframe.src
            // console.log(iframeSrc);
            iframe.src = iframeSrc;

            videoPlayBox.classList.remove('active')
        })
    })



}())


// hamburger.style.position = 'relative'